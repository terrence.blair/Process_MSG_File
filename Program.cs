﻿// TNB 9/7/2023
// Quick and dirty program to process .MSG files, extract the body and subject into a file with extension .TXT and 
// save each attachment to the file system for separate post-processing 

using System.Text;
using System.Text.RegularExpressions;
using MsgReader.Outlook;

namespace ExtractAttachmentsFromMsgFile
{
    class Program
    {
         static void Main(string[] args)
        {
            // Exit if wrong number of command line arguments
            ExitAppYN(args.Length != 2,"Process_MSG_File.EXE [FileMask] [Working Directory]");

            string fileMask = args[0];
            string workingDirectory = args[1].TrimEnd('\\');
            string filePath= new FileInfo(fileMask).Directory.FullName;
            string fileName = Path.GetFileName(fileMask);
            
            // Exit if working directory does not exist
            ExitAppYN(!Directory.Exists(workingDirectory), $"Unable to find working directory {workingDirectory}.");
            
            // Exit if file mask doesn't end in .MSG
            ExitAppYN(Path.GetExtension(fileMask).ToUpper() != ".MSG", "Expecting file mask ending in .MSG.");

            // Store unique filename representing the current email. It will be used to prepend the filenames to
            // to store the .TXT versions of the subject/body and each attachment.  The prepended string is 
            // the date and time that the email was received which, along with the name of the .MSG file should 
            // be sufficientl unique.
            string uniqueFilePrepend;

            // Process files indicated by file mask
            // Combine all text from the email (subject and body) into a single .TXT file
            // which can be used for tokenization
            string[] files = Directory.GetFiles(filePath, fileName);
            Log($"Processing {files.Length.ToString()} file(s) with mask {fileMask}...");
            
            // Process each file represented by the file mask
            foreach (string file in files)
            {
                Log("++++++++++++++++++++++++++++++");
                Log($"Processing file {file}...");
                using (var msg = new MsgReader.Outlook.Storage.Message(file))
                {
                    // Add the processing identifier (initialized above) to the beginning of the filename
                    // so all elements of the email can be identified once saved separately as multiple
                    // files in the working directory
                    uniqueFilePrepend = CleanAndFormatFileName(msg.SentOn.ToString()) + "_" + Path.GetFileNameWithoutExtension(file);
                    // textFileName is the text file containing the subject and message from the email
                    string textFileName = uniqueFilePrepend + ".TXT";
                    string fullTextFileName = Path.Combine(workingDirectory, textFileName);

                    // 1) Convert message body to text and save it in a text file
                    var sb = new StringBuilder();
                    sb.AppendLine(msg.Subject);
                    sb.AppendLine(msg.BodyText);
                    DeleteExistingFile(fullTextFileName);
                    File.WriteAllText(fullTextFileName, sb.ToString());
                    Log($"Created file  {fullTextFileName} containing subject and body text.");

                    // 2) Extract all attachments to working directory, prepending filename with same processing identifier
                    string attachmentFileName;
                    string fullAttachmentFileName;
                    foreach (var thisAttachment in msg.Attachments)
                    {
                        if (thisAttachment is Storage.Attachment attachment)
                        {
                            Log(".................");
                            Log($"Processing attachment {attachment.FileName}...");
                            attachmentFileName = uniqueFilePrepend + "_" + attachment.FileName;
                            fullAttachmentFileName = Path.Combine(workingDirectory, attachmentFileName);
                            DeleteExistingFile(fullAttachmentFileName);
                            Log($"Extracted attachment to {fullAttachmentFileName}.");
                            System.IO.File.WriteAllBytes(fullAttachmentFileName, attachment.Data);
                        }
                    }
                }
                Log($"Processed file {file}...");
            }
        }
        static void DeleteExistingFile(string fileName)
        // Delete fileName if it exists
        {
            if (File.Exists(fileName))
            {
                File.Delete(fileName);
            }
        }
        static string CleanAndFormatFileName(string input)
        // Replace characters in string input that would be invalid if used in a file name
        {
            // Define a regex pattern to match invalid characters in file names
            string invalidChars = new string(Path.GetInvalidFileNameChars());
            // Replace invalid characters with underscores
            string cleanedFileName = Regex.Replace(input, "[" + Regex.Escape(invalidChars) + "]", "_");
            cleanedFileName = Regex.Replace(cleanedFileName, @"[_\s]+", "_");
            return cleanedFileName;
        }
        public static void Log(string msg)
        // Write to the console standard out a message, prepended with the current date and time
        {
            Console.WriteLine($"{DateTime.Now:yyyy-MM-dd HH:mm:ss}: {msg}");
        }
        public static void ExitAppYN(bool exit, string msg)
        // Exit the application if the test condition = false
        {
            if (exit)
            {
                Console.WriteLine(msg);
                Environment.Exit(0);
            }
        }
    }
}